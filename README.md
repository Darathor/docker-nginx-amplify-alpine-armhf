Docker image for Nginx-amplify build on the lightweight linux distribution Alpine for raspberry Pi

**Software Stack** :
- Alpine 3.8
- Nginx 1.15.8
- Amplify  1.4.1-1

**Compatible :**
- Rasperry Pi Arm7 (tested)
- For other architecture : you can build from the dockerfile or use already built images from dockerhub.

***Installation Guide***

You just have to pull the image (59.4mb) from the gitlab registry:
```bash
docker pull registry.gitlab.com/darathor/docker-nginx-amplify-alpine-armhf
```


***More information***

To know more on how to use and configure amplify please see the original official nginx github project https://github.com/nginxinc/docker-nginx-amplify

